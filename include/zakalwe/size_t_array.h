#ifndef _Z_SIZE_T_ARRAY_
#define _Z_SIZE_T_ARRAY_

#include <zakalwe/array.h>

#ifdef __cplusplus
extern "C" {
#endif

Z_ARRAY_PROTOTYPE(size_t_array, size_t);

#ifdef __cplusplus
}
#endif

#endif
