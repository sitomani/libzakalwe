#ifndef _Z_CHAR_ARRAY_
#define _Z_CHAR_ARRAY_

#include <zakalwe/array.h>

#ifdef __cplusplus
extern "C" {
#endif

Z_ARRAY_PROTOTYPE(char_array, char);

#ifdef __cplusplus
}
#endif

#endif
